// Understanding Property Wrappers (Part. 2)

// We can use property wrappers to incluide all store logic into UserDefaults.

import Foundation

@propertyWrapper
struct UserDefault<T> {
    
    let key: String
    let defaultValue: T
    
    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}

enum GlobalSettings {
    
    // Property Wrapper do they work as computed variables. We use property wrapper constructor if we want to have default value.
    @UserDefault(key: "darkMode", defaultValue: false) static var darkMode: Bool
}

GlobalSettings.darkMode = true
GlobalSettings.darkMode
