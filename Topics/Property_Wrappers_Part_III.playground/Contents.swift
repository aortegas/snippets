// Understanding Property Wrappers (Part. 3)

// We can use property wrappers to incluide all store logic into UserDefaults (Other implementation).

import Foundation

private protocol AnyOptional {
    var isNil: Bool { get }
}


extension Optional: AnyOptional {
    var isNil: Bool { self == nil }
}


extension UserDefaults {
    
    static var shared: UserDefaults {
    
        let shared = UserDefaults.standard
        shared.addSuite(named: "group.aortegas.app")
        return shared
    }
}


@propertyWrapper
struct UserDefault<T> {
        
    // MARK - Properties
    private let key: String
    private let storage: UserDefaults
    private let defaultValue: T

    var wrappedValue: T {
        get {
            return storage.value(forKey: key) as? T ?? defaultValue
        }
        set {
            
            if let newValue = newValue as? AnyOptional, newValue.isNil {
                storage.removeObject(forKey: key)
            }
            else {
                storage.setValue(newValue, forKey: key)
            }
        }
    }

    // MARK: - Init
    // This init provides initialize values like this:
    // @UserDefault(key: "isFeatureToggleEnabled", defaultValue: true) var isFeatureToggleEnabled
    init(key: String, defaultValue: T, storage: UserDefaults = .shared) {
        
        self.key = key
        self.storage = storage
        self.defaultValue = defaultValue
    }
    
    // Init must have wrapperValue parameter if you want initialize values to property wrapper in this way:
    // @UserDefault(key: "myStringValue") var myStringValue = "Hi"
    init(wrappedValue defaultValue: T, key: String, storage: UserDefaults = .shared) {

        self.key = key
        self.storage = storage
        self.defaultValue = defaultValue
    }
    
    // If you define projectedValue, you can pass all property wrapper (not only its wrapped value).
    var projectedValue: UserDefault<T> { self }
}


extension UserDefault where T: ExpressibleByNilLiteral {

    // With this initializar you can init property wrapper in this way:
    // @UserDefault(key: "myOptionalInt") var myOptionalInt: Int?
    // IMPORTANT: Because T is not T?.
    init(key: String, storage: UserDefaults = .shared) {
        self.init(key: key, defaultValue: nil, storage: storage)
    }
}


struct GlobalSettings {
    
    // Property Wrapper type Bool is inferred with defaultValue value (true).
    @UserDefault(key: "isFeatureToggleEnabled", defaultValue: true) var isFeatureToggleEnabled
    
    // Property Wrapper type String is inferred with string "Hi". defaultValue == "Hi"
    @UserDefault(key: "myStringValue") var myStringValue = "Hi"
    
    // You can have optionasl property wrappers. See above cofiguration T to ExpressibleByNilLiteral.
    @UserDefault(key: "myOptionalInt") var myOptionalInt: Int?
}


// Usage 1: You can create property wrappers and set new values in them:
var globalSettings = GlobalSettings()

globalSettings.isFeatureToggleEnabled = false
print("isFeatureToggleEnabled: \(String(describing: globalSettings.isFeatureToggleEnabled))")

print("myStringValue: \(String(describing: globalSettings.myStringValue))")

print("myOptionalInt: \(String(describing: globalSettings.myOptionalInt))")
globalSettings.myOptionalInt = 5
print("myOptionalInt: \(String(describing: globalSettings.myOptionalInt))")
globalSettings.myOptionalInt = nil
print("myOptionalInt: \(String(describing: globalSettings.myOptionalInt))")

// Usage 2: You can pass property wrappers like a parameters of a function.
// You have two ways:

// 1. Pass all property wrapper with $, because projectedValue is defined in the property wrapper:
func passPropertyWrapper(_ userDefault: UserDefault<String>) {
    print("My wrapped value passing property wrapper: \(userDefault.wrappedValue)")
}

passPropertyWrapper(globalSettings.$myStringValue)

// 2. Pass only the wrapped value:
func passWrappedValue(_ wrappedValue: String) {
    print("My wrapped value passing property wrapped: \(wrappedValue)")
}

passWrappedValue(globalSettings.myStringValue)
