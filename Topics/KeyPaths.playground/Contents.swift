import Foundation

extension Array {

    func sorted<Value: Comparable>(keyPath: KeyPath<Element, Value>, by compareOperator: (Value, Value) -> Bool) -> [Element] {
        return sorted { compareOperator($0[keyPath: keyPath], $1[keyPath: keyPath]) }
    }
}

struct Videogame {
    var title: String
    var published: String
    var rating: Double
}

let games = [
    Videogame(title: "Cyberpunk 2077", published: "2020", rating: 999),
    Videogame(title: "Fallout 4", published: "2015", rating: 4.5),
    Videogame(title: "The Outer Worlds", published: "2019", rating: 4.4),
    Videogame(title: "RAGE", published: "2011", rating: 4.5),
    Videogame(title: "Far Cry New Dawn", published: "2019", rating: 4),
]

for game in games.sorted(keyPath: \Videogame.rating, by: >) {     // > is a compare operator
    print(game.title)
}


