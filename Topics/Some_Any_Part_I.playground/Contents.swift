/*
   
Some & Any (Part I)
===================

 - They are essential in protocol-oriented programming.
 - Both come to life when we are using generic protocols (AssociatedType) and have a similar use to work with these generic protocols.
 - 'some' defines 'opaque types' and 'any' defines 'existential types'.
 - The main difference between an opaque type and an existential type is the 'box' that existential types wrap. The 'box' allows us to store any concrete type within it, as long as the underlying type conforms to the specified protocol, allowing us to do something in existential types that an opaque types don't allow us to do. That is, what an opaque type does not allow us to do, an existential type does allow us to do. Let's see...

Some ('opaque types'):
 
 - It is necessary when, for example, for optimization or performance at compile time, we need to return only one of the types that can conform to the protocol.
 - Before "some", we had to return a specific type, not a protocol type, if we wanted to get the same result.
 - Limitations of 'some':
    - If a variable has been created with an object of type "some", another object of type "some" cannot be assigned to that variable, even if it is of the same type.
    - The above also applies to the creation of arrays.
    - In a calculated property or function, if our return type is "some", we cannot have the possibility at runtime to return two different types.
  
Any ('existential types'):

 - Limitations of 'any':
    - We cannot compare two 'any' types with equality, however we can do it with some types. This is because the compiler doesn't know what type is inside the box of existential types.
    - The only thing that the compiler can check is that the type of the protocol that accompanies 'any', conforms this protocol.
    - Existential types are less efficient than opaque types.
 
*/
 
import Foundation

protocol Vehicle {

  var name: String { get }

  associatedtype FuelType
  func fillGasTank(with fuel: FuelType)
}

struct Car: Vehicle {

  let name: String

  func fillGasTank(with fuel: Gasoline) {
    print("Fill \(name) with \(fuel.name)")
  }
}

struct Bus: Vehicle {

  let name: String

  func fillGasTank(with fuel: Diesel) {
    print("Fill \(name) with \(fuel.name)")
  }
}

struct Gasoline {
    let name = "gasoline"
}

struct Diesel {
    let name = "diesel"
}

let myAnyCar: any Vehicle = Car(name: "TestCar_1")
let mySomeCar: some Vehicle = Car(name: "TestCar_2")
let myAnyBus: any Vehicle = Bus(name: "TestBus_1")
let mySomeBus: some Vehicle = Bus(name: "TestBus_2")

print("myAnyCar: \(myAnyCar.name)")
print("mySomeCar: \(mySomeCar.name)")
print("myAnyBus: \(myAnyBus.name)")
print("mySomeBus: \(mySomeBus.name)")

//let gasoline = Gasoline()
//let diesel = Diesel()

// Error: member 'fillGasTank' cannot be used on value of type 'any Vehicle'; consider using a generic constraint instead
//myAnyCar.fillGasTank(with: gasoline)
//myAnyBus.fillGasTank(with: diesel)

// Error: cannot convert value of type 'Gasoline' to expected argument type '(some Vehicle).FuelType'
// Error: cannot convert value of type 'Diesel' to expected argument type '(some Vehicle).FuelType'
//mySomeCar.fillGasTank(with: gasoline)
//mySomeBus.fillGasTank(with: diesel)

// To see the solution for these problems, see Part II.
