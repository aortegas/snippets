/*
   
Result Builders
===============
 - Results builders are a very powerful capability that was added in Swift 5.4.
 - Basically, they are useful to improve our code, when we have pieces of code that build the same return type from several elements, we can replace these pieces with result builders, when we are going to need to write that piece of code frequently.
 - Result builders can also be explained as a Domain Specific Language (DSL) created to collect parts that are combined into a final result.
 - Result Builders in Swift allow you to create a result using "building blocks" lined up one after the other and are very popular as they are used for creating view stacks in SwiftUI.
 
*/
 
import UIKit
import PlaygroundSupport

// Imagine this code in which we get format for a text with NSAttributedString:

let paragraphStyleOne = NSMutableParagraphStyle()
paragraphStyleOne.alignment = .center
paragraphStyleOne.minimumLineHeight = CGFloat(36)

let paragraphStyleTwo = NSMutableParagraphStyle()
paragraphStyleTwo.alignment = .center
paragraphStyleTwo.minimumLineHeight = CGFloat(28)

let textFormattedOne = NSMutableAttributedString(string: "")
textFormattedOne.append(NSAttributedString(string: "Hola k ase",
                                           attributes: [.font: UIFont.systemFont(ofSize: 24),
                                                        .foregroundColor: UIColor.red,
                                                        .paragraphStyle: paragraphStyleOne]))

textFormattedOne.append(NSAttributedString(string: "\n\n"))

textFormattedOne.append(NSAttributedString(string: "with format",
                                           attributes: [.font: UIFont.systemFont(ofSize: 20),
                                                        .foregroundColor: UIColor.blue,
                                                        .paragraphStyle: paragraphStyleTwo]))


// We can do it with building block and syntax like this:

let textFormattedTwo = NSAttributedString {         // <- (*1) See above

    AttributedText("Hola k ase")                    // NSAttributedString Building block number 1
        .font(.systemFont(ofSize: 24))
        .foregroundColor(.red)
        .paragraphStyle {                           // <- (*2) See above
            
            ParagraphStyle()                        // NSParagraphStyle Building block number 1
                .alignment(.center)
                .minimumLineHeight(36.0)
        }

    EmptyLine()                                     // NSAttributedString Building block number 2

    AttributedText("with format")                   // NSAttributedString Building block number 3
        .font(.systemFont(ofSize: 20))
        .foregroundColor(.blue)
        .paragraphStyle {                           // <- (*2) See above
            
            ParagraphStyle()                        // NSParagraphStyle Building block number 1
                .alignment(.center)
                .minimumLineHeight(28.0)
        }
}


// (*1) So, let's start to see how to do it:

typealias Attributes = [NSAttributedString.Key: Any]

protocol AttributedTextType {
    var text: String { get }
    var attributes: Attributes { get }
    var attributedString: NSAttributedString { get }
}

extension AttributedTextType {

    var attributedString: NSAttributedString {
        NSAttributedString(string: text, attributes: attributes)
    }
}

// MARK: - Types
struct AttributedText: AttributedTextType {

    // MARK: - Properties
    let text: String
    let attributes: Attributes

    // MARK: - Add attributes functions.
    func font(_ font: UIFont) -> Self {
        attributes([.font: font])
    }
    
    func foregroundColor(_ color: UIColor) -> Self {
        attributes([.foregroundColor: color])
    }

    func paragraphStyle(@NSParagraphStyleBuilder _ builder: () -> NSParagraphStyle) -> Self {
        
        let paragraphStyle = builder()
        return attributes([.paragraphStyle: paragraphStyle])
    }
    
    // MARK: - Init
    private init(_ text: String, attributes: Attributes) {
        
        self.text = text
        self.attributes = attributes
    }

    init(_ text: String) {
        self.init(text, attributes: [:])
    }
    
    // MARK: - Private.
    private func attributes(_ newAttributes: Attributes) -> Self {              // Join the current attributes with new attributes and build a new AttributedTextType.
        
        let allAttributes = self.attributes.merging(newAttributes) { _, new in  // In case that we already had a key, we keep the new value.
            new
        }
        
        return AttributedText(self.text, attributes: allAttributes)
    }
}

// MARK: - New types
struct EmptyLine: AttributedTextType {
        
    // MARK: - Properties
    let text: String
    let attributes: Attributes

    // MARK: - Init
    init() {
        
        self.text = "\n\n"
        self.attributes = [:]
    }
}

@resultBuilder
enum NSAttributedStringBuilder {

    public static func buildBlock(_ components: AttributedTextType...) -> NSAttributedString {  // We have a variadic parameter to get one or several AttributedTextType.
                                                                                                // We make a building block and merge all AttributedTextType in one NSAttributedString.
        let textFormatted = NSMutableAttributedString(string: "")
        components.forEach { textFormatted.append($0.attributedString) }
        return textFormatted
    }
}

extension NSAttributedString {
    
    // MARK: - Init
    convenience init(@NSAttributedStringBuilder _ builder: () -> NSAttributedString) {    // (*1) New convenience init with building block.
        self.init(attributedString: builder())                                            // Building block gives us a NSAttributedString that is needed to designated init.
    }
}


// (*2) Let's see how to do paragraphStyle result builder:

protocol ParagraphStyleType {
    var paragraphStyle: NSParagraphStyle { get }
    func alignment(_ alignment: NSTextAlignment) -> Self
    func minimumLineHeight(_ value: Float) -> Self
}

// MARK: - Types
struct ParagraphStyle: ParagraphStyleType {

    // MARK: - Properties
    private var nsParagraphStyle: NSMutableParagraphStyle

    var paragraphStyle: NSParagraphStyle {
        nsParagraphStyle
    }
    
    // MARK: - Public
    func alignment(_ alignment: NSTextAlignment) -> Self {

        nsParagraphStyle.alignment = alignment
        return self
    }

    func minimumLineHeight(_ value: Float) -> Self {
        
        nsParagraphStyle.minimumLineHeight = CGFloat(value)
        return self
    }
    
    // MARK: - Init
    init() {
        self.nsParagraphStyle = NSMutableParagraphStyle()
    }
}

@resultBuilder
enum NSParagraphStyleBuilder {
    
    public static func buildBlock(_ components: ParagraphStyleType...) -> NSParagraphStyle {    // We have a variadic parameter to get one or several ParagraphStyleType.
        components.last?.paragraphStyle ?? NSParagraphStyle()                                   // We make a building block and get the last ParagraphStyleType to create a NSParagraphStyle.
    }
}


// Let's see how the View with UIKit:

class ViewController: UIViewController {
    
    override func loadView() {
        
        let view = UIView()
        view.backgroundColor = .white
        
        let labelOne = UILabel()
        labelOne.frame = CGRect(x: 100, y: 150, width: 175, height: 150)
        labelOne.numberOfLines = 0
        labelOne.backgroundColor = .darkGray
        labelOne.attributedText = textFormattedOne

        let labelTwo = UILabel()
        labelTwo.frame = CGRect(x: 100, y: 350, width: 175, height: 150)
        labelTwo.numberOfLines = 0
        labelTwo.backgroundColor = .darkGray
        labelTwo.attributedText = textFormattedTwo
        
        view.addSubview(labelOne)
        view.addSubview(labelTwo)
        self.view = view
    }
}

PlaygroundPage.current.liveView = ViewController()

