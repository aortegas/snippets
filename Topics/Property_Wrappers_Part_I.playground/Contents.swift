// Understanding Property Wrappers (Part. 1)

// Property wrappers are used because the structs do not have to manage the mutability of the data. So we extract mutability to a class,
// which is the data type that handles mutability well. In the first part we do it without property wrappers.

// Example 1. Without Property Wrappers.
import Foundation

class FIFOStackClass<T> {
    
    private var storage: [T] = []
    
    func addValue(value: T) {
        storage.insert(value, at: 0)
    }

    func extractValue() -> T {
        storage.removeFirst()
    }
}

struct FIFOStackStruct<T> {
    
    private let stack = FIFOStackClass<T>()
    
    func addValue(value: T) {
        stack.addValue(value: value)
    }
    
    func extractValue() -> T {
        stack.extractValue()
    }
}

let test1 = FIFOStackStruct<Int>()
test1.addValue(value: 10)
test1.addValue(value: 20)
test1.extractValue()
test1.extractValue()

// Example 2. Using Property wrappers.
@propertyWrapper
class FIFOStackClass2<T> {
    
    private var storage: [T] = []
    
    var wrappedValue: T {
        get {
            storage.removeFirst()
        }
        set {
            storage.insert(newValue, at: 0)
        }
    }
}

struct FIFOStackStruct2<T> {
    
    @FIFOStackClass2<T> private var stack    // Doesn't need initialization.
    
    func addValue(value: T) {
        stack = value
    }
    
    func extractValue() -> T {
        stack
    }
}

let test2 = FIFOStackStruct2<Int>()
test2.addValue(value: 10)
test2.addValue(value: 20)
test2.extractValue()
test2.extractValue()


