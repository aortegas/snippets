/*
 
 Some & Any (Part III)
 =====================

 - Resolving errors founded in part I, working with Some types.
 
*/

import Foundation

protocol Vehicle {

    var name: String { get }

    associatedtype FuelType: Fuel
    func fillGasTank(with fuel: FuelType)
}

struct Car: Vehicle {

    let name: String

    func fillGasTank(with fuel: Gasoline) {
        print("Fill \(name) with \(fuel.name)")
    }
}

struct Bus: Vehicle {

    let name: String

    func fillGasTank(with fuel: Diesel) {
        print("Fill \(name) with \(fuel.name)")
    }
}

protocol Fuel {

    var name: String { get }
    
    // Constrain `FuelType` to always equal to the type that conforms to the `Fuel` protocol.
    // See below the types that conform Fuel protocol, where the purchase() function returns its own type.
    // It's as if we are defining in the protocol the generation of a factory function, for this reason is a static function.
    associatedtype FuelType where FuelType == Self
    static func purchase() -> FuelType
}

struct Gasoline: Fuel {

    let name = "gasoline"
    
    static func purchase() -> Gasoline {
        
        print("Purchase gasoline from gas station.")
        return Gasoline()
    }
}

struct Diesel: Fuel {

    let name = "diesel"
    
    static func purchase() -> Diesel {
        
        print("Purchase diesel from gas station.")
        return Diesel()
    }
}


let mySomeCar: some Vehicle = Car(name: "TestCar_2")
let mySomeBus: some Vehicle = Bus(name: "TestBus_2")
print("mySomeCar: \(mySomeCar.name)")
print("mySomeBus: \(mySomeBus.name)")


let someVehicles: [some Vehicle] = [
    Car(name: "Car_1"),
    Car(name: "Car_2"),
    Car(name: "Car_3")
]

func fillGasTank(for vehicles: [some Vehicle]) {

    for vehicle in vehicles {

    // First Problem:
    // vehicle.fillGasTank(with: ????) // What to pass in here? We don't know its type of fuel.
    // We can solve like this:
    // 1. Create Fuel Protocol. (Done. See above)
    // 2. Implement Fuel Protocol in Gasoline and Diesel types. (Done. See above)
    // 3. Modify Vehicle protocol to identifify associatedtype FuelType with Fuel protocol. With last change we can get the fuel type for the vehicule. So we can do:
        let fuelTypeForVehicle = type(of: vehicle).FuelType.purchase()
        vehicle.fillGasTank(with: fuelTypeForVehicle)
    }
}

fillGasTank(for: someVehicles)
