import UIKit

let url = URL(string: "https://www.actualidadiphone.com/wp-content/uploads/2020/09/Swift-logo-1024x555.jpg")!

enum NetworkError: Error {
    case defaultError
    case notFound(Int)
    case notValid
}

// Managing asyncronous task with closures
// ---------------------------------------

func getImage(url: URL, completion: @escaping (Result<Data, NetworkError>) -> Void) {
    
    URLSession.shared.dataTask(with: url) { data, response, error in
        
        guard let data = data,
              let response = response as? HTTPURLResponse,
              error == nil else {
            
            completion(.failure(.defaultError))
            return
        }

        switch response.statusCode {
        case 200..<300:
            completion(.success(data))
        default:
            completion(.failure(.notFound(response.statusCode)))
        }
    }
    .resume()
}

print("Starting asyncronous task with closures...")
getImage(url: url, completion: { result in
    
    switch result {
    case .success(let data):
        let image = UIImage(data: data)!
        print("Ending asyncronous task with closures... image: \(image)")
    
    case .failure(let error):
        print("Ending asyncronous task with closures... error:: \(error)")
    }
})

// Inconvenients of use the completion closure:
// - Never forget execute completion().
// - Closures are harder to read.
// - Control to avoid retain cycles.



// Async / await
// -------------
// “Await is awaiting a callback from his buddy async”
// - Methods are linearly executed without going back and forth like you would with closures.

func getImageAsyncAwait(url: URL) async throws -> Result<Data, NetworkError> {      // In this case, we have a Result type like a return type
    
    let (data, response) = try await URLSession.shared.data(from: url)              // We use data instead of dataTask

    guard let response = response as? HTTPURLResponse else {
        return .failure(.defaultError)
    }
    
    switch response.statusCode {
    case 200..<300:
        return .success(data)
    default:
        return .failure(.notFound(response.statusCode))
    }
}

print("Starting asyncronous task with async / await ...")
Task {

    let result = try await getImageAsyncAwait(url: url)
    
    switch result {
    case .success(let data):
        let image = UIImage(data: data)!
        print("Ending asyncronous task with async / await ... image: \(image)")

    case .failure(let error):
        print("Ending asyncronous task with async / await ... error:: \(error)")
    }
}


// Converting asynchronous task handling with closures to async / await
// --------------------------------------------------------------------

func getImageWithConverting(url: URL) async throws -> Result<Data, NetworkError> {
    
    try await withCheckedThrowingContinuation({ continuation in                     // We have withCheckedContinuation function without throw exceptions
            
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data,
                  let response = response as? HTTPURLResponse,
                  error == nil else {
                
                return continuation.resume(returning: .failure(.defaultError))                      // We are replacing execution of completion closure by continuation (CheckedContinuation)
            }

            switch response.statusCode {
            case 200..<300:
                return continuation.resume(returning: .success(data))                               // We are replacing execution of completion closure by continuation (CheckedContinuation)
            default:
                return continuation.resume(returning: .failure(.notFound(response.statusCode)))     // We are replacing execution of completion closure by continuation (CheckedContinuation)
            }
        }
        .resume()
    })
}

print("Starting asyncronous task with async / await (converting closures example) ...")
Task {

    let result = try await getImageWithConverting(url: url)
    
    switch result {
    case .success(let data):
        let image = UIImage(data: data)!
        print("Ending asyncronous task with async / await (converting closures example) ... image: \(image)")

    case .failure(let error):
        print("Ending asyncronous task with async / await (converting closures example) ... error:: \(error)")
    }
}
