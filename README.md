
![](swift.png)

# Snippets

#### My snippets in Swift.

## Topics

#### 1 - KeyPaths

Understanding keypaths.

- [KeyPaths Playground](Topics/KeyPaths.playground/Contents.swift)


#### 2 - Property Wrappers (Part I)

Understanding Property Wrappers (Part I) - Manage Mutability.

- [Property Wrappers Playground](Topics/Property_Wrappers_Part_I.playground/Contents.swift)


#### 3 - Property Wrappers (Part II)

Understanding Property Wrappers (Part II) - UserDefaults example.

- [Property Wrappers Playground](Topics/Property_Wrappers_Part_II.playground/Contents.swift)


#### 4 - Property Wrappers (Part III)

Understanding Property Wrappers (Part III) - Initialize, optionals & wrapped values.

- [Property Wrappers Playground](Topics/Property_Wrappers_Part_III.playground/Contents.swift)


#### 5 - Async / Await

Understanding Async / Await.

- [Async / Await Playground](Topics/Async_Await.playground/Contents.swift)


#### 6 - Some & Any (Part I)

Understanding Some & Any (Part I).

- [Some & Any Playground](Topics/Some_Any_Part_I.playground/Contents.swift)


#### 7 - Some & Any (Part II)

Understanding Some & Any (Part II).

- [Some & Any Playground](Topics/Some_Any_Part_II.playground/Contents.swift)


#### 8 - Some & Any (Part III)

Understanding Some & Any (Part III).

- [Some & Any Playground](Topics/Some_Any_Part_III.playground/Contents.swift)


#### 9 - Result Builders

Understanding Result Builders.

- [Result_Builders Playground](Topics/Result_Builders.playground/Contents.swift)

